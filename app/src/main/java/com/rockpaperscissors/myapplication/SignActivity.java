package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class SignActivity extends Activity {

    EditText name;
    EditText pass;
    EditText passRepeat;
    TextView status;
    Boolean recaptchVerified = false;
    ImageView recaptcha;

    public static final String SITE_KEY = "6LdkoUoUAAAAABbC7aA8Y-Adv5dxrSauiC6Hs-0r";
    public static final String SITE_SECRET_KEY = "6LdkoUoUAAAAAKbvgAZfxeiYETcS6RQmlWeEI8Eg";
    String userResponseToken;

    public String getName() {
        return name.getText().toString();
    }

    public String getPass() {
        return pass.getText().toString();
    }

    public String getPassRep() {
        return passRepeat.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String languageToLoad = "en"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        //Toast.makeText(this,getApplicationContext().getResources().getConfiguration().locale.getDisplayLanguage(),Toast.LENGTH_LONG).show();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign);

        name = findViewById(R.id.input_name);
        pass = findViewById(R.id.input_password);
        passRepeat = findViewById(R.id.input_password_repeat);
        status = findViewById(R.id.textStatus);
        recaptcha = findViewById(R.id.verifyImg);
    }

    public void doRequest() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/register/goregister?login=" + getName() + "&haslo=" + getPass(), new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    status.setText(getMsg(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void SignUp(View view) {
        if (getName().equals("") || getPass().equals("") || getPassRep().equals(""))
            status.setText("Every field must be filled out!");
        else {
            if (getPass().equals(getPassRep())) {
                if (recaptchVerified) {
                    doRequest();
                } else {
                    status.setText("reCaptcha verfication failed");
                }

            } else status.setText("Entered passwords are different!");
        }
    }

    public String getMsg(String jsonStr) throws JSONException {

        JSONObject mJsonObject = new JSONObject(jsonStr);
        String msg = "";
        String status = mJsonObject.getString("status");

        if (status == "true") {
            Toast.makeText(getApplicationContext(), "Succesfully registered!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else msg = mJsonObject.getString("error_msg");

        return msg;
    }

    public void backToMenu(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    private void handleSiteVerify(String response) {

        Map<String, String> map = new HashMap<>();
        map.put("secret", SITE_SECRET_KEY);
        map.put("response", response);
        RequestParams params = new RequestParams(map);

        AsyncHttpClient client = new AsyncHttpClient();

        client.post("https://www.google.com/recaptcha/api/siteverify", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("success").equals("true")) {
                        Drawable imgVerify = SignActivity.this.getApplicationContext().getResources().getDrawable(
                                R.drawable.ic_action_check);
                        recaptcha.setImageDrawable(imgVerify);
                        recaptchVerified = true;
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                try {
                    String response = new String(responseBody, "UTF-8");
                    Toast.makeText(SignActivity.this, response, Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void recaptchaVerification(View view) {
        if (recaptchVerified) {
            Toast.makeText(this, "reCaptcha already verified!", Toast.LENGTH_LONG).show();
        } else {
            SafetyNet.getClient(this).verifyWithRecaptcha(SITE_KEY)
                    .addOnSuccessListener(this, response -> {
                        if (!response.getTokenResult().isEmpty()) {
                            handleSiteVerify(response.getTokenResult());
                        }
                    })
                    .addOnFailureListener(this, e -> {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Toast.makeText(getApplicationContext(), CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }
}

