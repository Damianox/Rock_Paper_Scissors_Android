package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class TournamentGameActivity extends Activity {

    TextView player1,player2,player1score,player2score,round,countdown,nextRoundIn,nextRoundText;
    static String player1name,player2name,meczId,resultOfRound;
    String loggedUser;
    int roundCount;
    boolean endGame;
    Button btnRock,btnPaper,btnScissors;
    int choiceRPS;
    int tournamentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tournament_game);
        endGame=false;
        roundCount=1;
        player1=findViewById(R.id.player1nameTournament);
        player1score=findViewById(R.id.player1scoreTournament);
        player2=findViewById(R.id.player2nameTournament);
        player2score=findViewById(R.id.player2scoreTournament);
        round=findViewById(R.id.txtRoundTournament);
        countdown=findViewById(R.id.txtTimeTournament);
        btnRock=findViewById(R.id.btn_rock_tournament);
        btnPaper=findViewById(R.id.btn_paper_tournament);
        btnScissors=findViewById(R.id.btn_scissors_tournament);

        player1name = getIntent().getStringExtra("player1");
        loggedUser = getIntent().getStringExtra("loggedUser");
        player2name = getIntent().getStringExtra("player2");
        tournamentID = getIntent().getIntExtra("tournamentID",0);

        meczId = getIntent().getStringExtra("meczId");
        round.setText("Round "+roundCount);

        player1.setText(player1name);
        player2.setText(player2name);

        Drawable imgRock = TournamentGameActivity.this.getApplicationContext().getResources().getDrawable(R.drawable.rock);
        imgRock.setBounds(0,0,(int) (imgRock.getIntrinsicWidth() * 0.1),(int) (imgRock.getIntrinsicHeight() * 0.1));
        btnRock.setCompoundDrawables(imgRock, null, null, null);

        Drawable imgPaper = TournamentGameActivity.this.getApplicationContext().getResources().getDrawable(R.drawable.paper);
        imgPaper.setBounds(0,0,(int) (imgPaper.getIntrinsicWidth() * 0.1),(int) (imgPaper.getIntrinsicHeight() * 0.1));
        btnPaper.setCompoundDrawables(imgPaper, null, null, null);

        Drawable imgScissors = TournamentGameActivity.this.getApplicationContext().getResources().getDrawable(R.drawable.scissors);
        imgScissors.setBounds(0,0,(int) (imgScissors.getIntrinsicWidth() * 0.1),(int) (imgScissors.getIntrinsicHeight() * 0.1));
        btnScissors.setCompoundDrawables(imgScissors, null, null, null);

        countdownToEnd();

        btnRock.setOnClickListener(v -> {
            btnPaper.setVisibility(View.INVISIBLE);
            btnScissors.setVisibility(View.INVISIBLE);
            choiceRPS=1;
            doPostChoiceRequest();
        });

        btnPaper.setOnClickListener(v -> {
            btnRock.setVisibility(View.INVISIBLE);
            btnScissors.setVisibility(View.INVISIBLE);
            choiceRPS=2;
            doPostChoiceRequest();
        });

        btnScissors.setOnClickListener(v -> {
            btnPaper.setVisibility(View.INVISIBLE);
            btnRock.setVisibility(View.INVISIBLE);
            choiceRPS=3;
            doPostChoiceRequest();
        });

        nextRoundText=findViewById(R.id.txtNextRoundTournament);
        nextRoundIn=findViewById(R.id.txtNextRoundStartsInTournament);
        nextRoundText.setVisibility(View.INVISIBLE);
        nextRoundIn.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
    }

    public void countdownToEnd(){
        if(!endGame) {
            btnPaper.setVisibility(View.VISIBLE);
            btnRock.setVisibility(View.VISIBLE);
            btnScissors.setVisibility(View.VISIBLE);
            choiceRPS = 0;
            round.setText("Round " + roundCount);

            new CountDownTimer(10500, 1000) {

                public void onTick(long millisUntilFinished) {
                    countdown.setText(Long.toString(millisUntilFinished / 1000));
                }

                public void onFinish() {
                    if (choiceRPS == 0) {
                        choiceRPS = 404;
                        doPostChoiceRequest();
                    }
                    doEndRoundRequest();
                    countdown.setText(resultOfRound);
                    countdownToNextRound();
                }
            }.start();
        }
    }

    public void countdownToNextRound(){
        if(!endGame) {
            nextRoundText.setVisibility(View.VISIBLE);
            nextRoundIn.setVisibility(View.VISIBLE);
            btnPaper.setVisibility(View.INVISIBLE);
            btnRock.setVisibility(View.INVISIBLE);
            btnScissors.setVisibility(View.INVISIBLE);
            new CountDownTimer(5500, 1000) {

                public void onTick(long millisUntilFinished) {
                    nextRoundIn.setText(Long.toString(millisUntilFinished / 1000));
                }

                public void onFinish() {
                    nextRoundText.setVisibility(View.INVISIBLE);
                    nextRoundIn.setVisibility(View.INVISIBLE);
                    roundCount++;
                    countdownToEnd();
                }
            }.start();
        }
    }

    public void doPostChoiceRequest() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/postchoice?login="+ loggedUser +"+&meczId="+meczId+"&rps="+choiceRPS+"&round="+roundCount, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    String status;
                    JSONObject mJsonObject = new JSONObject(response);
                    status = mJsonObject.getString("status");
                    if (status == "true") {
                        Toast.makeText(getApplicationContext(), "You chose "+choiceRPS, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "error chose " + choiceRPS, Toast.LENGTH_SHORT).show();
                        new CountDownTimer(1000, 1000) {

                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                doPostChoiceRequest();
                            }
                        };
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

//        final OkHttpClient client = new OkHttpClient();
//        final Request request = new Request.Builder()
//                .url("http://RockPaperScizor.eu-gb.mybluemix.net/game/postchoice?login="+player1name+"+&meczId="+meczId+"&rps="+choiceRPS+"&round="+roundCount)
//                .build();
//
//        final AsyncTask<Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>() {
//
//            protected String doInBackground(Void... params) {
//                try {
//                    Response response = client.newCall(request).execute();
//                    if (!response.isSuccessful()) {
//                        return null;
//                    }
//                    return response.body().string();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//
//                if (s != null) {
//                    String status = null;
//                    try {
//                        JSONObject mJsonObject = new JSONObject(s);
//                        status = mJsonObject.getString("status");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    if (status == "true") {
//                        Toast.makeText(getApplicationContext(), "You chose "+choiceRPS, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(getApplicationContext(), "error chose "+choiceRPS, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                else {
//                    Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG! \nNPLEASE TRY AGAIN", Toast.LENGTH_SHORT).show();
//                }
//            }
//        };
//
//        asyncTask.execute();
    }

    public void doEndRoundRequest() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/endround?tid="+tournamentID+"&meczId="+meczId+"&round="+roundCount, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String getwinner="";
                    String getwinner_round="";
                    String winner="";

                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = null;
                    try {
                        mJsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        getwinner = mJsonObject.getString("getwinner");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (getwinner.equals("game end")) {
                        endGame=true;
                        try {
                            winner = mJsonObject.getString("winner");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //String resultofmatch="";
                        Toast.makeText(getApplicationContext(), "game end", Toast.LENGTH_SHORT).show();

                        if(winner.equals(loggedUser)) {
                            Toast.makeText(getApplicationContext(), "YOU WON THE MATCH", Toast.LENGTH_SHORT).show();
                            //resultofmatch="YOU WON THE MATCH";
                            Intent intent = new Intent(TournamentGameActivity.this, GameEndingActivity.class);
                            intent.putExtra("resultOfmatch", "YOU WON THE MATCH");
                            intent.putExtra("username", loggedUser);
                            client.cancelAllRequests(true);
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "YOU LOST THE MATCH", Toast.LENGTH_SHORT).show();
                            //resultofmatch="YOU LOST THE MATCH";
                            Intent intent = new Intent(TournamentGameActivity.this, GameEndingActivity.class);
                            intent.putExtra("resultOfmatch", "YOU LOST THE MATCH");
                            intent.putExtra("username", loggedUser);
                            client.cancelAllRequests(true);
                            startActivity(intent);
                        }
                    }

                    else if (getwinner.equals("game in progress")) {
                        try {
                            getwinner_round = mJsonObject.getString("getwinner_round");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (getwinner_round == "draw") {
                            resultOfRound="DRAW!";
                            countdown.setText("DRAW!");
                            Toast.makeText(getApplicationContext(), "DRAW!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            String name_of_getwinner_round="";
                            try {
                                name_of_getwinner_round = mJsonObject.getString("getwinner_round");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(name_of_getwinner_round.equals(loggedUser)) {
                                resultOfRound = "YOU WON!";
                                Toast.makeText(getApplicationContext(), "YOU WON!", Toast.LENGTH_SHORT).show();
                                countdown.setText("YOU WON!");
                                int scorep1=0;
                                int scorep2=0;
                                try {
                                    scorep1 = mJsonObject.getInt(player1name);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    scorep2 = mJsonObject.getInt(player2name);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                player1score.setText(Integer.toString(scorep1));
                                player2score.setText(Integer.toString(scorep2));
                            }
                            else{
                                resultOfRound="YOU LOST!";
                                Toast.makeText(getApplicationContext(), "YOU LOST!", Toast.LENGTH_SHORT).show();
                                countdown.setText("YOU LOST!");
                                int scorep1=0;
                                int scorep2=0;
                                try {
                                    scorep1 = mJsonObject.getInt(player1name);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    scorep2 = mJsonObject.getInt(player2name);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                player1score.setText(Integer.toString(scorep1));
                                player2score.setText(Integer.toString(scorep2));
                            }
                        }
                    }
                    else if(getwinner.equals("game end draw"))
                    {
                        endGame=true;
                        Toast.makeText(getApplicationContext(), "MATCH IS A DRAW!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(TournamentGameActivity.this, GameEndingActivity.class);
                        String resultofmatch = "MATCH IS A DRAW";
                        intent.putExtra("resultOfmatch", resultofmatch);
                        intent.putExtra("username", loggedUser);
                        client.cancelAllRequests(true);
                        startActivity(intent);
                    }
                    else if(getwinner=="round in progress")
                        Toast.makeText(getApplicationContext(), "round in progresss!", Toast.LENGTH_SHORT).show();

                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

//        final OkHttpClient client = new OkHttpClient();
//        final Request request = new Request.Builder()
//                .url("http://RockPaperScizor.eu-gb.mybluemix.net/game/endround?meczId="+meczId+"&round="+roundCount)
//                .build();
//        final AsyncTask<Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>() {
//
//            protected String doInBackground(Void... params) {
//                try {
//                    Response response = client.newCall(request).execute();
//                    if (!response.isSuccessful()) {
//                        return null;
//                    }
//                    return response.body().string();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//
//                if (s != null) {
//                    Toast.makeText(getApplicationContext(), "endround requesy not null", Toast.LENGTH_SHORT).show();
//                    JSONObject mJsonObject = null;
//                    String getwinner="";
//                    String getwinner_round="";
//                    String winner="";
//                    int p1score=0; int p2score=0;
//                    try {
//                        mJsonObject = new JSONObject(s);
//                        winner = mJsonObject.getString("winner");
//                        getwinner = mJsonObject.getString("getwinner");
//                        getwinner_round = mJsonObject.getString("getwinner_round");
//                        p1score=mJsonObject.getInt("p1");
//                        p2score=mJsonObject.getInt("p2");
//
//
//                        if (getwinner == "game end") {
//                            Toast.makeText(getApplicationContext(), "game end", Toast.LENGTH_SHORT).show();
//                            if(winner==player1name) Toast.makeText(getApplicationContext(), "YOU WON THE MATCH", Toast.LENGTH_SHORT).show();
//                            else Toast.makeText(getApplicationContext(), "YOU LOST THE MATCH", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(GameActivity.this, MenuActivity.class);
//                            intent.putExtra("username", player1name);
//                            startActivity(intent);
//                        }
//
//                        if (getwinner == "game in progress") {
//                            if (getwinner_round == "draw") {
//                                resultOfRound="DRAW!";
//                                Toast.makeText(getApplicationContext(), resultOfRound, Toast.LENGTH_SHORT).show();
//                            }
//                            else{
//                                if(getwinner_round==player1name) {
//                                    resultOfRound = "YOU WON!";
//                                    Toast.makeText(getApplicationContext(), resultOfRound, Toast.LENGTH_SHORT).show();
//                                    tablep1score+=1;
//                                    player1score.setText(Integer.toString(tablep1score));
//                                    player2score.setText(Integer.toString(tablep2score));
//                                }
//                                else{
//                                    resultOfRound="YOU LOST!";
//                                    Toast.makeText(getApplicationContext(), resultOfRound, Toast.LENGTH_SHORT).show();
//                                    tablep2score+=1;
//                                    player1score.setText(Integer.toString(tablep1score));
//                                    player2score.setText(Integer.toString(tablep2score));
//                                }
//                            }
//                        }
//                        if(getwinner=="game end draw")
//                        {
//                            Toast.makeText(getApplicationContext(), "MATCH IS A DRAW!", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(GameActivity.this, MenuActivity.class);
//                            intent.putExtra("username", player1name);
//                            startActivity(intent);
//                        }
//                        if(getwinner=="round in progress")
//                            Toast.makeText(getApplicationContext(), "round in progresss!", Toast.LENGTH_SHORT).show();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else {
//                    Toast.makeText(getApplicationContext(), "SOMETHING WENT WRONG! \nPLEASE TRY AGAIN", Toast.LENGTH_SHORT).show();
//                }
//            }
//        };
//
//        asyncTask.execute();
    }
}

