package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class RankingActivity extends Activity {

    TableLayout table;
    TextView textView;
    String username;
    ProgressDialog  progressDialog;
    boolean listFull=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN );
        setContentView(R.layout.activity_ranking);
        textView = findViewById(R.id.textView);
        username=getIntent().getStringExtra("username");

        table = findViewById(R.id.tableRankingTournament);

        table.setColumnStretchable(0,true);
        table.setColumnStretchable(1,true);
        table.setColumnStretchable(2,true);
        table.setColumnStretchable(3,true);

        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Please wait ...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.whiteTransparent)));
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        loaded();
        doRequest();
    }


    public void backToMenu(View view) {
        Intent intent = new Intent(RankingActivity.this,RankingMenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    public void doRequest() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://rockpaperscizor.eu-gb.mybluemix.net/game/ranking", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    getRank(response);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void getRank (String jsonStr) throws JSONException {

        JSONArray mJsonArray = new JSONArray(jsonStr);

        for (int i=0; i < mJsonArray.length(); i++) {

            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
            TableRow row = new TableRow(this);
            TextView rank, name, wld, pts;
            rank = new TextView(this);
            name = new TextView(this);
            wld = new TextView(this);
            pts = new TextView(this);

            int place = i+1;
            String nameX = mJsonObject.getString("name");
            int wins = mJsonObject.getInt("wins");
            int losses = mJsonObject.getInt("losses");
            int draws = mJsonObject.getInt("draws");
            String gamesX = wins+"-"+losses+"-"+draws;
            int pointsX = mJsonObject.getInt("points");

            rank.setText(Integer.toString(place));
            rank.setTextSize(15);
            rank.setGravity(Gravity.CENTER);
            rank.setWidth(50);
            rank.setHeight(90);
            rank.setTextColor(getResources().getColor(R.color.colorWhite));
            rank.setBackgroundResource(R.drawable.back_table_left);

            name.setText(nameX);
            name.setTextSize(15);
            name.setGravity(Gravity.CENTER);
            name.setWidth(140);
            name.setHeight(90);
            name.setTextColor(getResources().getColor(R.color.colorWhite));
            name.setBackgroundResource(R.drawable.back_table_center);

            wld.setText(gamesX);
            wld.setTextSize(15);
            wld.setGravity(Gravity.CENTER);
            wld.setWidth(75);
            wld.setHeight(90);
            wld.setTextColor(getResources().getColor(R.color.colorWhite));
            wld.setBackgroundResource(R.drawable.back_table_center);

            pts.setText(Integer.toString(pointsX));
            pts.setTextSize(15);
            pts.setGravity(Gravity.CENTER);
            pts.setWidth(55);
            pts.setHeight(90);
            pts.setTextColor(getResources().getColor(R.color.colorWhite));
            pts.setBackgroundResource(R.drawable.back_table_right);

            row.addView(rank);
            row.addView(name);
            row.addView(wld);
            row.addView(pts);

            table.addView(row);
        }
        listFull=true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RankingActivity.this,RankingMenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    private void loaded()
    {
        if(listFull)
            progressDialog.dismiss();
        else{
            new CountDownTimer(1000, 1000) {

                public void onTick(long millisUntilFinished) {
                }
                public void onFinish() {
                    loaded();
                }
            }.start();
        }
    }
}
