package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class LadderActivity extends Activity {
    String username;
    int tournamentID;
    TableLayout ladder;
    TextView loadingLadder;
    Animation anim;
    CountDownTimer countDownTimer;
    CountDownTimer countDownTimerSearch;
    Button startSearch;
    String opponentName;
    Boolean isSearching = false;
    String matchID;

    Thread pBarThread;
    ProgressDialog progressDialog;
    int progress = 0;
    int countdown;
    Boolean back=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ladder);

        username = getIntent().getStringExtra("username");
        tournamentID = getIntent().getIntExtra("tournamentID", 0);
        ladder = findViewById(R.id.table_ladder);
        loadingLadder = findViewById(R.id.loadingLadderText);
        startSearch = findViewById(R.id.btn_startSearchTorunament);

        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(700); //You can manage the time of the blink with this parameter
        anim.setStartOffset(80);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        loadingLadder.startAnimation(anim);

        SpannableString ss1 = new SpannableString("Loading and synchronizing...");
        ss1.setSpan(new AbsoluteSizeSpan(20, true), 0, ss1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog = new ProgressDialog(this, R.style.MyProgressDialogStyle);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgressNumberFormat(null);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.whiteTransparent)));
        progressDialog.setMessage(ss1);
        progressDialog.setCancelable(false);

        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                getLadder();
            }
        }.start();
    }

    public void stopSearchTorunament() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/stopsearch?tid=" + tournamentID + "&name=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "false") {
                        Toast.makeText(getApplicationContext(), mJsonObject.getString("error_msg"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Search stopped!", Toast.LENGTH_SHORT).show();
                        isSearching = false;
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    public void startSearchTorunament(View view) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/startsearch?tid=" + tournamentID + "&name=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "false") {
                        Toast.makeText(getApplicationContext(), mJsonObject.getString("error_msg"), Toast.LENGTH_SHORT).show();
                    } else {
                        startSearch.setVisibility(View.INVISIBLE);
                        loadingLadder.setText("WAITING FOR OPPONENT");
                        loadingLadder.startAnimation(anim);
                        countDownTimerSearch = new CountDownTimer(2000, 1000) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                getOpponent();
                                if(back) countDownTimer.cancel();
                            }
                        }.start();
                        isSearching = true;
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    private void getLadder() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/ladder?tid=" + tournamentID, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "false") {
                        Toast.makeText(getApplicationContext(), mJsonObject.getString("error_msg"), Toast.LENGTH_SHORT).show();
                        countDownTimer = new CountDownTimer(3000, 1000) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                getLadder();
                                if(back) countDownTimer.cancel();
                            }
                        }.start();
                    } else {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        anim.cancel();
                        createLadder(response);
                    }
                } catch (JSONException | UnsupportedEncodingException e) {
                    try {
                        String response = new String(responseBody, "UTF-8");
                        anim.cancel();
                        createLadder(response);
                    } catch (JSONException | UnsupportedEncodingException ex) {
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    private void createLadder(String jsonStr) throws JSONException {

        JSONArray mJsonArray = new JSONArray(jsonStr);

        for (int i = 0; i < mJsonArray.length(); i++) {

            JSONObject mJsonObject = mJsonArray.getJSONObject(i);
            TableRow row = new TableRow(this);
            TableRow rowSpace = new TableRow(this);
            TextView player1 = new TextView(this);
            TextView player2 = new TextView(this);
            TextView vs = new TextView(this);
            TextView space = new TextView(this);

            String name1 = mJsonObject.getString("name1");
            String name2 = mJsonObject.getString("name2");

            player1.setText(name1);
            player1.setTextSize(20);
            player1.setGravity(Gravity.CENTER);
            player1.setWidth(140);
            player1.setHeight(90);
            player1.setTextColor(getResources().getColor(R.color.colorWhite));
            player1.setBackgroundResource(R.drawable.back_table_left);

            player2.setText(name2);
            player2.setTextSize(20);
            player2.setGravity(Gravity.CENTER);
            player2.setWidth(140);
            player2.setHeight(90);
            player2.setTextColor(getResources().getColor(R.color.colorWhite));
            player2.setBackgroundResource(R.drawable.back_table_right);

            vs.setText("vs");
            vs.setTextSize(20);
            vs.setGravity(Gravity.CENTER);
            vs.setWidth(40);
            vs.setHeight(90);
            vs.setTextColor(getResources().getColor(R.color.colorWhite));
            vs.setBackgroundResource(R.drawable.back_table_center);

            if (name1.equals(username)) {
                player1.setTypeface(player1.getTypeface(), Typeface.BOLD);
                player2.setTypeface(player1.getTypeface(), Typeface.BOLD);
                vs.setTypeface(player1.getTypeface(), Typeface.BOLD);
                row.setBackgroundResource(R.drawable.back_table_rounded_red_border);
                matchID = mJsonObject.getString("meczID");
                matchID = matchID.replace("\"", "");
                opponentName = name2;

            } else if (name2.equals(username)) {
                player1.setTypeface(player1.getTypeface(), Typeface.BOLD);
                player2.setTypeface(player1.getTypeface(), Typeface.BOLD);
                vs.setTypeface(player1.getTypeface(), Typeface.BOLD);
                row.setBackgroundResource(R.drawable.back_table_rounded_red_border);
                matchID = mJsonObject.getString("meczID");
                matchID = matchID.replace("\"", "");
                opponentName = name1;
            }

            space.setGravity(Gravity.CENTER);
            space.setWidth(150);
            space.setHeight(90);

            row.addView(player1);
            row.addView(vs);
            row.addView(player2);
            rowSpace.addView(space);

            ladder.addView(row);
            ladder.addView(rowSpace);
            loadingLadder.setText("LADDER LOADED");
        }
    }

    private void getOpponent() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://rockpaperscizor.eu-gb.mybluemix.net/tournament/getopponent?tid=" + tournamentID + "&login=" + username + "+&opplogin=" + opponentName + "&matchid=" + matchID, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "true") {
                        anim.cancel();
                        Intent intent = new Intent(getApplicationContext(), TournamentGameActivity.class);
                        intent.putExtra("player1", mJsonObject.getString("player1"));
                        intent.putExtra("player2", mJsonObject.getString("player2"));
                        intent.putExtra("meczId", matchID);
                        intent.putExtra("loggedUser", username);
                        intent.putExtra("tournamentID", tournamentID);
                        long timeNow = System.currentTimeMillis();
                        long timeGameStart = mJsonObject.getLong("time_start");
                        long timeGameEnd = mJsonObject.getLong("time_end");

                        countdown = (int) timeGameStart - (int) timeNow;
                        progressDialog.setMax(countdown);

                        progressDialog.show();
                        Handler pBarThreadPost = new Handler();
                        pBarThread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    while (progress < countdown) {
                                        progressDialog.setProgress(progress);
                                        sleep(1);
                                        ++progress;
                                    }
                                    pBarThreadPost.post(() -> startActivity(intent));
                                } catch (InterruptedException ignored) {
                                }
                            }
                        };
                        pBarThread.start();
                        pBarThreadPost.removeCallbacksAndMessages(null);

                    } else {
                        String error_msg = mJsonObject.getString("error_msg");
                        Toast.makeText(getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();
                        new CountDownTimer(3000, 1000) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                getOpponent();
                            }
                        }.start();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    public void backToMenu(View view) {
        back=true;
        Intent intent = new Intent(this, TournamentMenuActivity.class);
        intent.putExtra("username", username);
        startActivity(intent);
        if (isSearching)
            stopSearchTorunament();
    }

    @Override
    public void onBackPressed() {
        back=true;
        Intent intent = new Intent(this, TournamentMenuActivity.class);
        intent.putExtra("username", username);
        startActivity(intent);
        if (isSearching)
            stopSearchTorunament();
    }

}

/*
[{"name1":"test","name2":"test2","meczID":"\"1_p2-4/T\""},
{"name1":"test1,"name2":"test3","meczID":"\"1_p2-4/T\""},
{"name1":"test5","name2":"test4","meczID":"\"1_p2-4/T\""},
{"name1":"test6","name2":"test5","meczID":"\"2_p2-3/T\""}]
 */

/*
getopponent
{"tag":"getopponent",
"status":true,
"player1":"test4",
"player2":"test",
"meczId":"1_p2-5\/T",
"time_start":"1526991692867",
"time_end":"1526991699867"}
 */
