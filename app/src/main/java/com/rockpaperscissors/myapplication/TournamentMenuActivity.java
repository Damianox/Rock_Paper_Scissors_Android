package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class TournamentMenuActivity extends Activity {
    String username;
    EditText editTextMAXplayers;
    ListView listViewTournaments;
    List<String> listofTournaments;
    List<Integer> listofTournamentsID;
    ArrayAdapter<String> adapter;
    int tournamentID;

    Handler handler;
    Runnable runnableCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tournament_menu);

        username = getIntent().getStringExtra("username");
        editTextMAXplayers = findViewById(R.id.inputMaxPlayers);
        listViewTournaments = findViewById(R.id.listTournaments);
        listofTournaments = new ArrayList<>();
        listofTournamentsID = new ArrayList<>();

        try {
            loadListofTournaments();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listViewTournaments.setOnItemClickListener((parent, view, position, id) -> {
            tournamentID = listofTournamentsID.get(position);
            signToTournaments(tournamentID);
        });

        adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listofTournaments) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);
                // Initialize a TextView for ListView each Item
                TextView tv = view.findViewById(android.R.id.text1);
                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.WHITE);
                // Generate ListView Item using TextView
                return view;
            }
        };
    }

    public void signToTournaments(int id) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/signup?tid=" + id + "&name=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String msg;
                    String status = mJsonObject.getString("status");

                    if (status == "true") {
                        Toast.makeText(getApplicationContext(), "Signed to tournament id=" + id, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), LadderActivity.class);
                        intent.putExtra("username", username);
                        intent.putExtra("tournamentID", tournamentID);
                        startActivity(intent);
                    } else {
                        msg = mJsonObject.getString("error_msg");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    public void loadListofTournaments() throws JSONException {
        listofTournaments.clear();
        listofTournamentsID.clear();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/tournaments", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONArray mJsonArray = new JSONArray(response);
                    for (int i = 0; i < mJsonArray.length(); i++) {

                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);

                        int id = mJsonObject.getInt("Id");
                        int max_players = mJsonObject.getInt("max_players");
                        int now_round = mJsonObject.getInt("now_round");
                        int zapisy = mJsonObject.getInt("zapisy");

                        listofTournamentsID.add(id);
                        listofTournaments.add("ID: " + String.valueOf(id) + " Max: " + String.valueOf(max_players) + " Round: " + String.valueOf(now_round) + " Signs: " + String.valueOf(zapisy));
                    }
                    listViewTournaments.setAdapter(adapter);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    public void createTournament(View view) {
        int liczba;
        liczba = Integer.valueOf(editTextMAXplayers.getText().toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/tournament/new?maxplayers=" + liczba, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String msg;
                    String status = mJsonObject.getString("status");
                    if (status == "true") {
                        Toast.makeText(getApplicationContext(), "Tournament has been created!", Toast.LENGTH_SHORT).show();
                    } else {
                        msg = mJsonObject.getString("error_msg");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    public void backToMenu(View view) {
        Intent intent = new Intent(this, PlayMenuActivity.class);
        intent.putExtra("username", username);
        if (handler != null) handler.removeCallbacks(runnableCode);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, PlayMenuActivity.class);
        intent.putExtra("username", username);
        if (handler != null) handler.removeCallbacks(runnableCode);
        startActivity(intent);
    }

    public void refreshTournaments(View view) {
        try {
            loadListofTournaments();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


/*
https://rockpaperscizor.eu-gb.mybluemix.net/tournament/tournaments

[{"Id":1,"winner_id":0,"max_players":1,"now_round":0,"zapisy":1},
{"Id":2,"winner_id":0,"max_players":2,"now_round":0,"zapisy":1},
{"Id":3,"winner_id":0,"max_players":4,"now_round":0,"zapisy":1},
{"Id":4,"winner_id":0,"max_players":4,"now_round":0,"zapisy":1},
{"Id":5,"winner_id":0,"max_players":4,"now_round":0,"zapisy":1},
{"Id":6,"winner_id":0,"max_players":4,"now_round":0,"zapisy":1}]


http://RockPaperScizor.eu-gb.mybluemix.net/tournament/ladder?tid="+tournamentID
[{"name1":"test","name2":"test3","meczID":"\"1_p2-4/T\""},
{"name1":"test1","name2":"test2","meczID":"\"2_p2-3/T\""}]

http://RockPaperScizor.eu-gb.mybluemix.net/tournament/startsearch?tid="+tournamentID+"&name="+username
{"tag":"signup","status":true}
 */


