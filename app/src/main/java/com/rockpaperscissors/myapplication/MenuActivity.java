package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;


public class MenuActivity extends Activity {

    String username="";
    TextView usernameDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN );
        setContentView(R.layout.activity_menu);

        usernameDisplay = findViewById(R.id.usernameDisplay);
        username=getIntent().getStringExtra("username");
        usernameDisplay.setText(username);
    }

    public void playClick(View view) {
        Intent intent = new Intent(MenuActivity.this,PlayMenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    public void rankingClick(View view) {
        Intent intent = new Intent(MenuActivity.this,RankingMenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    public void logoutClick(View view) {
        doLogoutRequest();
    }

    public void doLogoutRequest() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/login/gologout?login=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    getMsg(response);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void getMsg(String jsonStr) throws JSONException {

        JSONObject mJsonObject = new JSONObject(jsonStr);
        String msg="";
        String status = mJsonObject.getString("status");

        if(status=="true")
        {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        else
        {
            msg=mJsonObject.getString("error_msg");
            Toast.makeText(this,msg,Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onBackPressed() { }


    public void chatClick(View view) {
        Intent intent = new Intent(this,ChatActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }
}
