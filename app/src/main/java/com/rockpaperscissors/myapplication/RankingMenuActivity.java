package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class RankingMenuActivity extends Activity {
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN );
        setContentView(R.layout.activity_ranking_menu);

        username = getIntent().getStringExtra("username");

    }

    public void ranking1on1(View view) {
        Intent intent = new Intent(this,RankingActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    public void rankingTournament(View view) {
        Intent intent = new Intent(this,RankingTournamentActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    public void backToMenu(View view) {
        Intent intent = new Intent(this,MenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,MenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }
}
