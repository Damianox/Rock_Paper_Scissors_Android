package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

public class GameStartsInActivity extends Activity {

    TextView TimeToGame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN );
        setContentView(R.layout.activity_game_starts_in);

        TimeToGame=findViewById(R.id.txtTimeToGame);

        new CountDownTimer(5500, 1000) {

            public void onTick(long millisUntilFinished) {
                TimeToGame.setText(Long.toString(millisUntilFinished / 1000));
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(900); //You can manage the time of the blink with this parameter
                anim.setStartOffset(40);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                TimeToGame.startAnimation(anim);
            }

            public void onFinish() {
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra("player1", getIntent().getStringExtra("player1"));
                intent.putExtra("player2", getIntent().getStringExtra("player2"));
                intent.putExtra("meczId", getIntent().getStringExtra("meczId"));
                intent.putExtra("loggedUser", getIntent().getStringExtra("loggedUser"));
                startActivity(intent);
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
    }

}
