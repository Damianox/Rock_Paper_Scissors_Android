package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class PlayActivity extends Activity {

    static String username;
    TextView searchingOpponent;
    Button startButton;
    Button stopButton;
    Button returnToMenuPlay;
    Animation anim;
    Handler handler;
    boolean searchStopped;

    Thread pBarThread;
    private int progress = 0;
    ProgressDialog progressDialog;

    int countdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play);

        searchStopped = false;
        username = getIntent().getStringExtra("username");
        searchingOpponent = findViewById(R.id.searchingOpponent);
        startButton = findViewById(R.id.btn_startSearch);
        stopButton = findViewById(R.id.btn_stopSearch);
        stopButton.setVisibility(View.INVISIBLE);
        searchingOpponent.setVisibility(View.INVISIBLE);
        returnToMenuPlay = findViewById(R.id.btn_returnToMenuPlay);
        handler = new Handler();

        SpannableString ss1 = new SpannableString("Loading and synchronizing...");
        ss1.setSpan(new AbsoluteSizeSpan(20, true), 0, ss1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog = new ProgressDialog(this, R.style.MyProgressDialogStyle);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgressNumberFormat(null);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.whiteTransparent)));
        progressDialog.setMessage(ss1);
        progressDialog.setCancelable(false);

    }


    public void backToMenu(View view) {
        Intent intent = new Intent(this, PlayMenuActivity.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }

    public void startSearch(View view) {
        searchStopped = false;
        startButton.setVisibility(View.INVISIBLE);
        stopButton.setVisibility(View.VISIBLE);
        returnToMenuPlay.setVisibility(View.INVISIBLE);
        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(700); //You can manage the time of the blink with this parameter
        anim.setStartOffset(80);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        searchingOpponent.startAnimation(anim);
        doStartRequest();
    }

    public void stopSearch(View view) {
        searchingOpponent.clearAnimation();
        searchingOpponent.setVisibility(View.INVISIBLE);
        startButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.INVISIBLE);
        returnToMenuPlay.setVisibility(View.VISIBLE);
        searchStopped = true;
        doStopRequest();
    }

    public void doStartRequest() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/game/startsearch?login=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "true") {
                        Toast.makeText(getApplicationContext(), "Searching started!", Toast.LENGTH_SHORT).show();
                        new CountDownTimer(5000, 1000) {

                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                doGetOpponentRequest();
                            }
                        }.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "Searching not started!", Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void doStopRequest() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/game/stopsearch?login=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "true") {
                        Toast.makeText(getApplicationContext(), "Searching stopped!", Toast.LENGTH_SHORT).show();
                        client.cancelAllRequests(true);
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        Toast.makeText(getApplicationContext(), "Searching not stopped!", Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void doGetOpponentRequest() {

        final AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/game/getopponent?login=" + username, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    JSONObject mJsonObject = new JSONObject(response);
                    String status = mJsonObject.getString("status");
                    if (status == "true") {
                        searchingOpponent.setText("Opponent found");

                        String player1 = mJsonObject.getString("player1");
                        String player2 = mJsonObject.getString("player2");
                        String meczId = mJsonObject.getString("meczId");
                        Toast.makeText(getApplicationContext(), "Opponent found!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                        intent.putExtra("player1", player1);
                        intent.putExtra("player2", player2);
                        intent.putExtra("meczId", meczId);
                        intent.putExtra("loggedUser", username);

                        long timeNow = System.currentTimeMillis();
                        long timeGameStart = mJsonObject.getLong("time_start");
                        long timeGameEnd = mJsonObject.getLong("time_end");

                        countdown = (int) timeGameStart - (int) timeNow;
                        progressDialog.setMax(countdown);

                        progressDialog.show();
                        Handler pBarThreadPost = new Handler();
                        pBarThread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    while (progress < countdown) {
                                        progressDialog.setProgress(progress);
                                        sleep(1);
                                        ++progress;
                                    }
                                    pBarThreadPost.post(() -> startActivity(intent));
                                } catch (InterruptedException e) {
                                }
                            }
                        };
                        pBarThread.start();
                        pBarThreadPost.removeCallbacksAndMessages(null);
                        //doStopRequest();

                    } else if (status == "false" && !searchStopped) {
                        String error_msg = mJsonObject.getString("error_msg");
                        Toast.makeText(getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();
                        new CountDownTimer(6000, 1000) {

                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                doGetOpponentRequest();
                            }
                        }.start();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        doStopRequest();
        Intent intent = new Intent(this, PlayMenuActivity.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }

}
