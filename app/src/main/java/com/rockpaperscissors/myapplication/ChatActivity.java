package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends Activity {

    FirebaseAuth auth;
    DatabaseReference databaseReference;
    ListAdapter listAdapter;
    ListView listOfMessages;
    ArrayList<ChatMessage> arrayListMsg;
    String username="";
    ProgressDialog  progressDialog;
    Boolean userAuth,listFull;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chat);

        userAuth=false;
        listFull=false;
        listOfMessages = findViewById(R.id.list_of_messages);
        username=getIntent().getStringExtra("username");
        arrayListMsg = new ArrayList<>();
        loaded();

        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Please wait ...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.whiteTransparent)));
        progressDialog.setCancelable(false);
        progressDialog.show();

        databaseReference=FirebaseDatabase.getInstance().getReference();
        auth = FirebaseAuth.getInstance();
        auth.signInAnonymously()
                .addOnCompleteListener(this, (Task<AuthResult> task) -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = auth.getCurrentUser();
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(username).build();
                        user.updateProfile(profileUpdates);
                        userAuth=true;
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(getApplicationContext(), "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                displayChatMessages(dataSnapshot);
                listFull=true;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ImageView send = findViewById(R.id.send);
        send.setOnClickListener(view -> {
            EditText input = findViewById(R.id.input);
            FirebaseDatabase.getInstance()
                    .getReference()
                    .push()
                    .setValue(new ChatMessage(input.getText().toString(),
                            FirebaseAuth.getInstance()
                                    .getCurrentUser()
                                    .getDisplayName())
                    );
            input.setText("");
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseAuth.getInstance().signOut();
    }

    private void displayChatMessages(DataSnapshot dataSnapshot) {
        arrayListMsg.clear();
        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setMessageText(ds.getValue(ChatMessage.class).getMessageText());
            chatMessage.setMessageTime(ds.getValue(ChatMessage.class).getMessageTime());
            chatMessage.setMessageUser(ds.getValue(ChatMessage.class).getMessageUser());
            arrayListMsg.add(chatMessage);
        }

        listAdapter = new ListAdapter(this,R.layout.message,arrayListMsg);
        listOfMessages.setAdapter(listAdapter);
        listOfMessages.setSelection(listOfMessages.getAdapter().getCount()-1);
    }

    public void backToMenu(View view) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this,MenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this,MenuActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    private void loaded()
    {
        if(userAuth && listFull)
            progressDialog.dismiss();
        else{
            new CountDownTimer(1000, 1000) {

                public void onTick(long millisUntilFinished) {
                }
                public void onFinish() {
                    loaded();
                }
            }.start();
        }
    }
}


class ListAdapter extends ArrayAdapter<ChatMessage> {

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapter(Context context, int resource, List<ChatMessage> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.message, null);
        }

        ChatMessage p = getItem(position);

        if (p != null) {
            TextView tt1 = v.findViewById(R.id.message_text);
            TextView tt2 = v.findViewById(R.id.message_user);
            TextView tt3 = v.findViewById(R.id.message_time);

            if (tt1 != null) {
                tt1.setText(p.getMessageText());
            }

            if (tt2 != null) {
                tt2.setText(p.getMessageUser());
            }

            if (tt3 != null) {
                tt3.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",p.getMessageTime()));
            }
        }

        return v;
    }

}
