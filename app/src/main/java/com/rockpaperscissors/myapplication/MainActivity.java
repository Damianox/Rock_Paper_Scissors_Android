package com.rockpaperscissors.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends Activity {

    public EditText name;
    public EditText pass;
    public TextView loginStatus;

    public String getName() {
        return name.getText().toString();
    }
    public String getPass() {
        return pass.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.input_name);
        pass = findViewById(R.id.input_password);
        loginStatus = findViewById(R.id.loginStatus);
    }

    public void openRegistration(View view) {
        if (!isOnline())
            Toast.makeText(getApplicationContext(), "NO INTERNET CONNECTION! \n PLEASE TRY AGAIN", Toast.LENGTH_SHORT).show();
        else {
            Intent intent = new Intent(MainActivity.this, SignActivity.class);
            startActivity(intent);
        }
    }

    public void clickLogin(View view) {
        if (!isOnline())
            Toast.makeText(getApplicationContext(), "NO INTERNET CONNECTION! \n PLEASE TRY AGAIN", Toast.LENGTH_SHORT).show();
        else {
            if (getName().equals("") || getPass().equals(""))
                loginStatus.setText("Every field must be filled out!");
            else {
                doLoginRequest();
            }
        }
//        Intent intent = new Intent(MainActivity.this, LadderActivity.class);
//        intent.putExtra("username", getName());
//        startActivity(intent);
    }

    public void doLoginRequest() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://RockPaperScizor.eu-gb.mybluemix.net/login/gologin?login=" + getName() + "&haslo=" + getPass(), new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, "UTF-8");
                    getMsg(response);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void getMsg(String jsonStr) throws JSONException {

        JSONObject mJsonObject = new JSONObject(jsonStr);
        String msg = "";
        String status = mJsonObject.getString("status");

        if (status == "true") {
            loginStatus.setText(status);
            Intent intent = new Intent(MainActivity.this, MenuActivity.class);
            intent.putExtra("username", getName());
            startActivity(intent);
        } else
            msg = mJsonObject.getString("error_msg");
        loginStatus.setText(msg);
    }

    @Override
    public void onBackPressed() {
        finish();
        moveTaskToBack(true);
    }

    public void exit(View view) {
        finish();
        moveTaskToBack(true);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
